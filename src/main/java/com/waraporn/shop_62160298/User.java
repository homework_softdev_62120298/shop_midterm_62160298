/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waraporn.shop_62160298;

/**
 *
 * @author Acer
 */
public class User {
   
    private String ID;
    private String Name;
    private String Brand;
    private double Price;
    private int Amount;


    User(String ID, String Name, String Brand, double Price, int Amount) {
        this.ID = ID;
        this.Name = Name;
        this.Brand = Brand;
        this.Price = Price;
        this.Amount = Amount;
    }

    User(String Id, String Name, String Brand, String Price, String Amount) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    User(String string, String watermelon, String kfc, int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    public String getID() {
        return ID;
    }
    public void setID(String ID) {
        this.ID = ID;
    }
    public String getName() {
        return Name;
    }
    public void setName(String Name) {
        this.Name = Name;
    }
    public String getBrand() {
        return Brand;
    }
    public void setBrand(String Brand) {
        this.Brand = Brand;
    }
    public double getPrice() {
        return this.Price;
    }
    public void setPrice(double Price) {
        this.Price = Price;
    }
    public int getAmount() {
        return Amount;
    }
    public void setAmount(int Amount) {
        this.Amount = Amount;
    }
    public String toString(){
        return "ID = " + ID + ", name = " + Name + ", Brand = " + Brand
                + ", Price = " + Price + ", Amount = " + Amount ;
    }

}
