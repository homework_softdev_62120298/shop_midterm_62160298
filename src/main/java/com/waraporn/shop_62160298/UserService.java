/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waraporn.shop_62160298;

import java.util.ArrayList;

/**
 *
 * @author Acer
 */
public class UserService {
    private static ArrayList<User> userList = new ArrayList<>();
    private static User currentUser = null;

    // Create
    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    // Delete
    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    // Delete User
    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }
    // Read

    public static ArrayList<User> gerUsers() {
        return userList;
    }

    public static User getUser(int index) {
        return userList.get(index);
    }
    // Update

    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }

    public static boolean isLogin() {
        return currentUser != null;
    }

    public static User getCurrentUser() {
        return currentUser;
    }
}
